from django.conf.urls import patterns, url
urlpatterns = patterns('',
    url(r'^user_profile/$', 'auth_lifecycle.view__profile.get_rendered',
        name='user_profile'),
)
