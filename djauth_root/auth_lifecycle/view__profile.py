"""Renders web pages for the user-authentication-lifecycle project."""
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers       import reverse_lazy
from django.shortcuts               import render
from django.template                import RequestContext

TMPL_BASE_DIR = 'auth_lifecycle/'
"""
The root of the (relative) template paths used in this file, as exists
in

/home/myname/django_files/django_auth_lifecycle/auth_lifecycle/templates/

Equal to 'auth_lifecycle/'

This rendundant 'auth_lifecycle' directory is recommended, because it
allows the templates to be 'namespaced'.
- https://docs.djangoproject.com/en/1.7/topics/http/urls/#url-namespaces
- https://docs.djangoproject.com/en/1.7/topics/http/urls/#topics-http-reversing-url-namespaces
"""

PROFILE_LOGGED_OUT_REDIRECT_URL_NAME='main_page'
"""
TEMPORARY VALUE, only while the login view does not exist. Once it's
created, change this to 'login', and eliminate this comment. This
is also used by the tests.

The 'main_page' will be created in the next post. The 'login' page
will be created in the post after that.
"""
@login_required(login_url=reverse_lazy(PROFILE_LOGGED_OUT_REDIRECT_URL_NAME))
def get_rendered(request):
    """
    Displays information unique to the logged-in user.

    This blindly passes the request context back to the template.

    Before the login functionality exists, you can only view page in a
    browser by commenting out the 'login_required' decorator, and adding
    following three lines before the return:

    from django.contrib.auth import authenticate, login
    user = authenticate(username='admin', password='admin')
    login(request, user)

    - https://docs.djangoproject.com/en/1.7/topics/auth/default/#how-to-log-a-user-in
    - https://docs.djangoproject.com/en/1.7/topics/auth/default/#django.contrib.auth.decorators.login_required

    Regarding `reverse_lazy`:
    - http://stackoverflow.com/questions/26446718/decorated-view-causing-a-viewdoesnotexist-error
    """
    return  render(request, TMPL_BASE_DIR + 'user_profile.html',
                   context_instance=RequestContext(request))
