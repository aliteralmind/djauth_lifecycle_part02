"""
Tests for the user-profile view.

DEPENDS ON TEST:     test__utilities.py
DEPENDED ON BY TEST: None

To run the tests in this file:
    1. source /home/myname/django_files/django_auth_lifecycle/djauth_venv/bin/activate
    2. cd /home/myname/django_files/django_auth_lifecycle/djauth_root/
    3. python -Wall manage.py test auth_lifecycle.test__view_profile

See the top of <link to .test__utilities> for more information.
"""
from .test__utilities         import assert_attr_val_in_content
from .test__utilities         import create_insert_test_users, debug_test_user
from .test__utilities         import login_get_next_user, TEST_PASSWORD
from .test__utilities         import UserFactory
from .view__profile           import PROFILE_LOGGED_OUT_REDIRECT_URL_NAME
from django.core.urlresolvers import reverse
from django.test              import TestCase

def _subtest_next_logged_in_user(test_instance):
    """
    All private information for a single *already-logged-in* test user,
    should be displayed somewhere on the page.

    Private function for this file only.
    """

    #Log in and get the test user, and assert the login succeeded.
    test_user = login_get_next_user(test_instance)

    #The page should load successfully.
    response = test_instance.client.get(reverse('user_profile'))
    test_instance.assertEqual(200, response.status_code)

    #Convert bytes to string.
    content_str = str(response.content)

    #test_user's attributes should exist somewhere on the page
    assert_attr_val_in_content(test_instance, 'username', test_user.username, content_str)
    assert_attr_val_in_content(test_instance, 'birth_year', test_user.profile.birth_year, content_str)
    assert_attr_val_in_content(test_instance, 'email', test_user.email, content_str)
    assert_attr_val_in_content(test_instance, 'first_name', test_user.first_name, content_str)
    assert_attr_val_in_content(test_instance, 'last_name', test_user.last_name, content_str)

class ProfilePageTestCase(TestCase):
    """Tests for the user profile page."""
    def setUp(self_ignored):
        """Insert test users."""
        create_insert_test_users()

    def test_profile_redirects_when_not_logged_in(self):
        """
        Page should redirect when a non-logged-in user attempts to access
        it.
        """
        self.client.logout()
        #"follow=True" is required because we're testing a redirect.
        #- https://code.djangoproject.com/ticket/10971
        response = self.client.get(reverse('user_profile'), follow=True)

        #http://stackoverflow.com/questions/7949089/how-to-find-the-location-url-in-a-django-response-object/22073938#22073938

        #Exactly one redirect expected
        self.assertTrue(len(response.redirect_chain) == 1)

        #Get the first (which, since there's exactly one, could also be
        #'[-1]')
        last_url, status_code = response.redirect_chain[0]
        expected_url_mid = reverse(PROFILE_LOGGED_OUT_REDIRECT_URL_NAME) + '?next='
        self.assertTrue(expected_url_mid in last_url)

        #http://en.wikipedia.org/wiki/HTTP_302
        self.assertEqual(status_code, 302)

    def test_profile_logged_in(self):
        """
        Private information for logged-in users should be somewhere
        on the page.
        """

        #Test the first two users
        for  n in range(2):
            _subtest_next_logged_in_user(self)
