"""
Defines a single extra user-profile field for the user-authentication
lifecycle demo project: Birth year. There is no validation on this field.
"""
from django.contrib.auth.models import User
from django.db                  import models

class UserProfile(models.Model):
    """
    Extra information about a user: Birth year.

    ---NOTES---

    Useful related SQL:
    - `select id from auth_user where username <> 'admin';`
    - `select * from auth_lifecycle_userprofile where user_id=(x,x,...);`
    """
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User, related_name="profile")

    # The additional attributes we wish to include.
    birth_year = models.IntegerField(
        blank=True,
        verbose_name="Year you were born")

    # Override the __str__() method to return out something meaningful
    def __str__(self):
        return self.user.username
