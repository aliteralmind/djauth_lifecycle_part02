"""Passes browser requests to the proper view, based on the url."""
from django.conf.urls import patterns, include, url
from django.contrib   import admin

urlpatterns = patterns('',
    #Adding the namespace attribute to this element would force all
    #references to be prefixed with "auth_lifecycle:". In the template:
    #    {% url 'auth_lifecycle:url_name' %}
    #Elsewhere:
    #    reverse('auth_lifecycle:url_name')
    #
    #- https://docs.djangoproject.com/en/1.7/topics/http/urls/#url-namespaces
    #- https://docs.djangoproject.com/en/1.7/topics/http/urls/#topics-http-reversing-url-namespaces
    #
    #(Unrelated note: If this were a multi-line comment, it would cause
    # a syntax error.)
    url(r'^auth_lifecycle/', include('auth_lifecycle.urls')),
        # namespace="auth_lifecycle")),
    url(r'^admin/', include(admin.site.urls)),
)
