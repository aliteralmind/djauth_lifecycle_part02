mkdir -p /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/dalt02_venv/
sudo virtualenv -p /usr/bin/python3.4 /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/dalt02_venv/
source /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/dalt02_venv/bin/activate
sudo /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/dalt02_venv/bin/pip install django
sudo /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/dalt02_venv/bin/pip install gunicorn
sudo /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/dalt02_venv/bin/pip install psycopg2
sudo chown -R jeffy /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/
mkdir /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/djauth_root/
django-admin.py startproject django_auth_lifecycle /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/djauth_root
cd /home/jeffy/django_files/djauth_lifecycle_tutorial/part_02/djauth_root/
python manage.py startapp auth_lifecycle

git init
git remote add origin https://aliteralmind@bitbucket.org/aliteralmind/djauth_lifecycle_part02.git

cp /home/jeffy/django_files/djauth_lifecycle_tutorial/part_01/.git/info/exclude .git/info
cp /home/jeffy/django_files/djauth_lifecycle_tutorial/part_01/git_add_commit_push_master*.sh .
chmod 774 git_add_commit_push_master*.sh

#---
#
#Steps in previous parts:
sudo ${venvDir}bin/pip install factory_boy
