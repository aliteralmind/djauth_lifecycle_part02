# For use in all project sandboxes.
# Save this in the project's root directory,
# with the name:
#    git_add_commit_push.bat

# call z_backup_..._data_folder_delete_existing_first.bat

#branch=$1
#commit_msg=$2
branch=master
commit_msg=$1

echo "The commit message is the one and only command-line parameter."
echo "About to do"
echo "1.  git add --all :/"
echo "2.  git commit -m \"$commit_msg\""
echo "3.  git push -u origin $branch"
#read -p "Press a key to proceed."

git add --all
#read -p "Press a key to proceed."
#'$commit_msg' literally prints the variable name:
git commit -m "$commit_msg"
#read -p "Press a key to proceed."
git push -u origin $branch
